magic-wormhole for Debian
-------------------------

This package follows the upstream tarballs from PyPI, not from the git
repository, starting from 0.8.2-1. Upstream decided this is the
official tarball that would be signed with OpenPGP certificates so we
will use that.

Unfortunately that tarball has extra files created by the `setup.py
sdist` command which are not in the original git archive, so we can't
track the git history anymore.

To import and build a new release:

   uscan
   gbp import-orig ../magic-wormhole_0.8.2.orig.tar.gz
   gbp buildpackage

The package is configured to use git-buildpackage but should be also
buildable using ordinary Debian tools.

Also, because I am also contributing upstream, the branch layout may
look a bit unusual to Debian developers:

 * upstream: tarballs imported from upstream (result of git import-orig)
 * debian: debian package development branch
 * master: upstream master development branch

 -- Antoine Beaupré <anarcat@debian.org>, Thu, 15 Dec 2016 10:53:07 -0500

spake2==0.8
pynacl
six
attrs>=16.3.0
twisted[tls]>=17.5.0
autobahn[twisted]>=0.14.1
automat
hkdf
tqdm>=4.13.0
click
humanize
txtorcon>=18.0.2

[:sys_platform=="win32"]
pywin32

[dev]
mock
tox
pyflakes
magic-wormhole-transit-relay==0.1.2
magic-wormhole-mailbox-server==0.3.1

[dilate]
noiseprotocol
